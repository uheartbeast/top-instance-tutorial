{
    "id": "8e931918-7cb2-49be-8102-c7937139fc48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_object",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "21e61158-49f2-4168-aee6-539c84ff860a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e931918-7cb2-49be-8102-c7937139fc48",
            "compositeImage": {
                "id": "9a904ff5-c9a5-4a21-81a5-226bb087070c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e61158-49f2-4168-aee6-539c84ff860a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa32bf0-00f7-44ab-9c4d-5b9057d0571b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e61158-49f2-4168-aee6-539c84ff860a",
                    "LayerId": "f0d37bb1-73f3-46f0-a548-fd9959286c55"
                }
            ]
        },
        {
            "id": "7db618b9-f894-40ff-9ad4-a08d5b98a0dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e931918-7cb2-49be-8102-c7937139fc48",
            "compositeImage": {
                "id": "c28a8de0-6765-4439-8935-44dd4c0159f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db618b9-f894-40ff-9ad4-a08d5b98a0dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8dc0ea-598f-42fa-980c-cc28ed4e29b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db618b9-f894-40ff-9ad4-a08d5b98a0dd",
                    "LayerId": "f0d37bb1-73f3-46f0-a548-fd9959286c55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f0d37bb1-73f3-46f0-a548-fd9959286c55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e931918-7cb2-49be-8102-c7937139fc48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}