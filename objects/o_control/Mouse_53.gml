with (o_object) {
	selected = false;
}

var instance = top_instance_position(mouse_x, mouse_y, o_object);
if instance_exists(instance) {
	instance.selected = true;
}