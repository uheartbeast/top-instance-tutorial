{
    "id": "2ad7707b-10a9-4a2d-a4cf-65538ed8bff8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_object",
    "eventList": [
        {
            "id": "ad22b8a7-cf35-4647-bf7f-3025474c1daf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ad7707b-10a9-4a2d-a4cf-65538ed8bff8"
        },
        {
            "id": "9e6cbf2b-8f91-4ab0-b0ae-79ec9225d352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ad7707b-10a9-4a2d-a4cf-65538ed8bff8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8e931918-7cb2-49be-8102-c7937139fc48",
    "visible": true
}